# Wellcome to TestApp

This repository contains the static TestApp website.

**If you want to acces the old JavaScript frontend, use the `old-stable` branch.**

[TestApp](https://testapp.ga/) is an educational open-source project for **writing class tests digitally** on smartphones or computers. We offer **easy-to-use class management**, **individual** class tests and random **practice** tests. Students can register **individually** or a **hole school/class** can use TestApp.

Our source code is hosted on [GitLab](https://gitlab.com/testapp-system).

![TestApp screenshot](https://dev.testapp.gaassets/img/Screenshot_dashboard.png)

## More Information

Visit [gitlab.com/testapp-system/testapp-flutter](https://gitlab.com/testapp-system/testapp-flutter) for more details.

## Support development

You can support us by coding. Just ask to become member of [GitLab TestApp group](https://gitlab.com/testapp-system).

*For running our servers and deploying mobile apps we need $200. Help us!*

Feel free to donate: [Buy me a coffee](https://www.buymeacoffee.com/JasMich) or BTC: *3NUiJXDCkyRTb9Tg7n63yK6Y7CexADtSEh*
